package bgmc;

import bgmc.utils.Stats;

public class ModelCheckingResult {
	private boolean wasModelValid;
	private Stats stats;
	
	public ModelCheckingResult(boolean wasModelValid, Stats stats) {
		this.wasModelValid = wasModelValid;
		this.stats = stats;
	}
	
	public boolean wasModelValid() {
		return wasModelValid;
	}
	
	public Stats getStats() {
		return stats;
	}
}
