package bgmc.property

import collection.Set
import bgmc.property.utils.CaseClassHashcodeCaching

case class Transition(label: Set[BoolExpression], unacceptingSets: Set[Until], dest: UbaState) extends CaseClassHashcodeCaching
case class TransitionToUbaQ(dest: UbaQuantifierState) extends CaseClassHashcodeCaching