package bgmc.property

import bgmc.property.utils.JavaHashMapStore
import bgmc.BaState

object LtlQAwareBaState {
  def fromFormula(f: Formula): BaState = {
    val initUbaState = new UbaState(collection.mutable.Set(f), new JavaHashMapStore[UbaState], new JavaHashMapStore[UbaQuantifierState])
    new UbaDegeneralizingBaState(initUbaState, Set.empty[Until], new JavaHashMapStore[UbaDegeneralizingBaState])
  }
}