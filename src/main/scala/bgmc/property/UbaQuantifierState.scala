package bgmc.property

import collection.mutable.{Set, HashSet}
import bgmc.ModelState
import scala.collection.mutable.ListBuffer
import bgmc.property.utils.Store
import bgmc.ObjectVal

// Instances of this class are flyweights in the context of a single qStatesStore.
// Immutable.
class UbaQuantifierState(
    private val untilsToSatisfy: HashSet[Until], 
    private val rightsOfUntils: HashSet[Formula], 
    private val literals: HashSet[BoolExpression], 
    private val nexts: HashSet[Formula], 
    private val quantifiers: HashSet[QuantifierFormula], 
    private val statesStore: Store[UbaState], 
    private val qStatesStore: Store[UbaQuantifierState]
) {
  private val activeQuantifier = quantifiers.head
  
  private val cachedTransitionsByInvolvedObjs = collection.mutable.HashMap.empty[collection.immutable.Set[ObjectVal], TransitionsTuple]
  
  def transitionsFor(modelState: ModelState): TransitionsTuple = {
    val rangedOverObjs = activeQuantifier.range.evaluate(modelState).objects
    
    val rangedOverObjsSet = rangedOverObjs.toSet
    cachedTransitionsByInvolvedObjs.get(rangedOverObjsSet) match {
      case Some(transitions) => transitions
      case None =>
        val rewrittenQuantifier = rewriteActiveQuantifierFor(modelState, rangedOverObjs)
        val transitions =  newExpanderFor(rewrittenQuantifier).expand
        
        cachedTransitionsByInvolvedObjs(rangedOverObjsSet) = transitions
        transitions
    }
  }
  
  private def rewriteActiveQuantifierFor(modelState: ModelState, rangedOverObjVals: Iterable[ObjectVal]): Formula = {
    val rangedOverObjIds = rangedOverObjVals map {_.getId}
    
    val formulasWithVarReplaced = ListBuffer.empty[Formula]
    for(oId <- rangedOverObjIds)
      formulasWithVarReplaced += replaceAllVarsWithObjectId(activeQuantifier.formula, activeQuantifier.varId, ObjectId(oId))
    
    activeQuantifier match {
      case UniversalQuantifier(_, _, _) =>
        if(rangedOverObjIds.size > 0)
        	formulasWithVarReplaced reduce {And(_, _)}
        else Literal(True)
      case ExistentialQuantifier(_, _, _) =>
        if(rangedOverObjIds.size > 0)
          formulasWithVarReplaced reduce {Or(_, _)}
        else Literal(False)
    }
  }
  
  private def replaceAllVarsWithObjectId(formula: Formula, varId: VariableId, oId: ObjectId): Formula = {
    def r_(formula: Formula): Formula =
      formula match {
        case Literal(exp) => Literal(r_e(exp))
        case Next(f) => Next(r_(f))
        case And(l, r) => And(r_(l), r_(r))
        case Or (l, r) => Or (r_(l), r_(r))
        case Until(l, r) => Until(r_(l), r_(r))
        case Release(l, r) => Release(r_(l), r_(r))
        case UniversalQuantifier  (variable, range, f) => UniversalQuantifier  (variable, r_dotted(range), r_(f))
        case ExistentialQuantifier(variable, range, f) => ExistentialQuantifier(variable, r_dotted(range), r_(f))
      }
    
    def r_e(bExpr: BoolExpression): BoolExpression =
      bExpr match {
        case bool @ (True | False) => bool
        case Neg(expr) => Neg(r_e(expr))
        case AsBoolExpr(dottedExpr) => AsBoolExpr(r_dotted(dottedExpr))
        case Comparison(op, l, r) => Comparison(op, r_dotted(l), r_dotted(r))
      }
    
    def r_dotted(dotted: DottedExpr): DottedExpr =
      dotted match {
      	case DottedExpr(`varId`, rest) => DottedExpr(oId, rest)
      	case _ => dotted
      }
    
    r_(formula)
  }
  
  private def newExpanderFor(todo: Formula) = 
    new Expander(
      HashSet(todo), statesStore, qStatesStore,
      untilsToSatisfy.clone,
      rightsOfUntils.clone,
      literals.clone,
      nexts.clone,
      quantifiers - activeQuantifier
    )
  
  override def equals(otherObj: Any) = {
    val other = otherObj.asInstanceOf[UbaQuantifierState]
    
    untilsToSatisfy == other.untilsToSatisfy &&
    rightsOfUntils == other.rightsOfUntils &&
    literals == other.literals &&
    nexts == other.nexts &&
    quantifiers == other.quantifiers
  }
  
  override lazy val hashCode = {
    val prime = 41
    prime * (prime * (prime * (prime * (prime + untilsToSatisfy.hashCode) + rightsOfUntils.hashCode) + literals.hashCode) + nexts.hashCode) + quantifiers.hashCode
  }
}