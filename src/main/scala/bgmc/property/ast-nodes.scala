package bgmc.property

import bgmc.{ModelState, ObjectVal}
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import bgmc.property.utils.CaseClassHashcodeCaching

sealed abstract class Formula extends CaseClassHashcodeCaching with Product {
  var isRightOfAnUntil = false
}

case class Literal(expr: BoolExpression) extends Formula
case class Next(formula: Formula) extends Formula
case class And(left: Formula, right: Formula) extends Formula
case class Or (left: Formula, right: Formula) extends Formula
case class Until(left: Formula, right: Formula) extends Formula {
  right.isRightOfAnUntil = true
}
case class Release(left: Formula, right: Formula) extends Formula

sealed abstract class QuantifierFormula extends Formula {
  def varId: VariableId
  def range: DottedExpr
  def formula: Formula
}

case class UniversalQuantifier  (varId: VariableId, range: DottedExpr, formula: Formula) extends QuantifierFormula
case class ExistentialQuantifier(varId: VariableId, range: DottedExpr, formula: Formula) extends QuantifierFormula

sealed case class DottedExpr(first: DottedExprFirst, fieldChain: List[String]) extends CaseClassHashcodeCaching {	// TODO: Move to another file if possible.
  def evaluate(modelState: ModelState): DottedExprEvaluation = {
    var objs = first.evaluate(modelState)
    if(! fieldChain.isEmpty)
      for(field <- fieldChain) {
        objs = if(field == "_size") {
          Seq(objAsObjectVal(new Integer(objs.size)))
        } else if(field == "_flatten_size") {
          Seq(objAsObjectVal(new Integer(objs.map{_.getValue.asInstanceOf[java.util.Collection[Any]].size}.sum)))
        } else {
          val newObjs = ListBuffer.empty[ObjectVal]
          for(obj <- objs) {
            newObjs += obj getField field
            if(obj == null)
        	    throw new TypingError(s"Object of class ${obj.getClassId} does not have field $field.")
          }
          newObjs
        }
      }
      new DottedExprEvaluation(this, objs)
  }
  
  override def toString() = (ListBuffer(first.id) ++ fieldChain).mkString(".")
  
  private def objAsObjectVal[T <: AnyRef](obj: T) = new ObjectVal {
	override def getValue = obj
    override def getField(f: String) = throw new UnsupportedOperationException
    override def getClassId = throw new UnsupportedOperationException
    override def getId = throw new UnsupportedOperationException
  } 
}

class DottedExprEvaluation(dottedExpr: DottedExpr, objs: Iterable[ObjectVal]) {
  def objects = objs
  def objectIds = objs map {_.getId}
  def toSingle[T]: T = {
    if(objs.size != 1)
      throw new TypingError(s"Expected a single object in dotted expression: ${dottedExpr.toString}.\nBut got: ${objs.size}.")
    try {
      objs.head.getValue.asInstanceOf[T]
    } catch {
      case e: ClassCastException =>
        throw new TypingError(s"Could not cast the result of dotted expression ${dottedExpr.toString} to the required type.\n${e.getMessage}")
    }
  }
}

sealed abstract class DottedExprFirst {
  def evaluate(modelState: ModelState): Iterable[ObjectVal]	// TODO: Rename to something better.
  def id: String
}
case class ClassId(id: String) extends DottedExprFirst {
  def evaluate(modelState: ModelState): Iterable[ObjectVal] =
    modelState.getObjectsByClassId(id).asScala
}
case class ObjectId(id: String) extends DottedExprFirst {
  def evaluate(modelState: ModelState): Iterable[ObjectVal] = {
    var obj = modelState.getObjectById(id)
    if(obj == null)
      throw new ObjectNotFoundError(id)
    List(obj)
  }
}

case class VariableId(id: String) extends DottedExprFirst {
    def evaluate(modelState: ModelState): Iterable[ObjectVal] =
      throw new UnboundVariableEncounteredError(id)
}

sealed abstract class BoolExpression extends CaseClassHashcodeCaching with Product

case object True extends BoolExpression
case object False extends BoolExpression
case class Neg(bExpr: BoolExpression) extends BoolExpression
case class AsBoolExpr(dotted: DottedExpr) extends BoolExpression
case class Comparison(op: ComparisonOperator, l: DottedExpr, r: DottedExpr) extends BoolExpression
sealed abstract class ComparisonOperator
case object Equality extends ComparisonOperator
case object Unequality extends ComparisonOperator
case object LessThan extends ComparisonOperator
case object GreaterThan extends ComparisonOperator