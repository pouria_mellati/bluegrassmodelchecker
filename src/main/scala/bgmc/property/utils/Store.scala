package bgmc.property.utils

import java.util.HashMap

trait Store[T] {
  def add(elem: T)
  def getExistingOrNone(elem: T): Option[T]
  def size: Long
}

class JavaHashMapStore[T] extends Store[T] {
  val store = new HashMap[T, T](100000)
  
  def add(elem: T) {
    store.put(elem, elem)
  }
  
  def getExistingOrNone(elem: T): Option[T] = {
    val stored = store.get(elem)
    if(stored != null)
      Some(stored)
    else
      None
  }
  
  def size: Long = store.size
}