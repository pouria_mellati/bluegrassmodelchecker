package bgmc.property.utils

// Only immutable case classes may mix this trait in.
trait CaseClassHashcodeCaching {
  self: Product =>
    override lazy val hashCode: Int = scala.runtime.ScalaRunTime._hashCode(this)
}