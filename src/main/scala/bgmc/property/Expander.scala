package bgmc.property

import collection.mutable.{Set, HashSet, Buffer}
import bgmc.property.utils.Store

/* Expands the set of formulas passed as todo. The expand() function can only be called once for each
 * instance of Expander.
 */
class Expander(val todo: Set[Formula], val statesStore: Store[UbaState], val qStatesStore: Store[UbaQuantifierState],
  val untilsToSatisfy: HashSet[Until]             = HashSet.empty[Until], 
  val rightsOfUntils:  HashSet[Formula]           = HashSet.empty[Formula],
  val literals:        HashSet[BoolExpression]    = HashSet.empty[BoolExpression],
  val nexts:           HashSet[Formula]           = HashSet.empty[Formula],
  val quantifiers:     HashSet[QuantifierFormula] = HashSet.empty[QuantifierFormula] 
) {
  
  def expand(): TransitionsTuple = {
    val transitions = HashSet.empty[Transition]
    val transitionsToUbaQ = HashSet.empty[TransitionToUbaQ]
    
    while(! todo.isEmpty) {
	    val formula = todo.head
	    todo -= formula
	    
	    if(formula.isRightOfAnUntil)
	      rightsOfUntils += formula
	    
	    formula match {
	      case Literal(expr) =>
	        literals += expr
	      case Next(form) =>
	        nexts += form
	      case quantifier: QuantifierFormula =>
	        quantifiers += quantifier
	      case And(left, right) =>
	        todo += (left, right)
	      case Or(left, right) =>
	        val splitOff = this.copied
	        splitOff.todo += right
	        val splitOffsResult = splitOff.expand()
	        transitions ++= splitOffsResult.transitionsToUba
	        transitionsToUbaQ ++= splitOffsResult.transitionsToUbaQ
	        todo += left
	      case until @ Until(left, right) =>
	        untilsToSatisfy += until
	        todo += Or(And(left, Next(until)), right)
	      case release @ Release(left, right) =>
	        todo += Or(And(right, Next(release)), And(left, right))
	    }
	}
    
    if(quantifiers.isEmpty) {
      val unSatisfiedUntils = untilsToSatisfy filter {until: Until => ! rightsOfUntils.contains(until.right)}
      
      val newUbaState = new UbaState(nexts, statesStore, qStatesStore)
      statesStore.getExistingOrNone(newUbaState) match {
        case Some(existing) =>
          transitions += Transition(label = literals, unacceptingSets = unSatisfiedUntils, dest = existing)
        case None =>
          statesStore.add(newUbaState)
          transitions += Transition(label = literals, unacceptingSets = unSatisfiedUntils, dest = newUbaState)
      }
    } else {
      val newUbaQState = this.asUbaQuantifierState
      qStatesStore.getExistingOrNone(newUbaQState) match {
        case Some(existing) =>
          transitionsToUbaQ += TransitionToUbaQ(existing)
        case None =>
          qStatesStore.add(newUbaQState)
          transitionsToUbaQ += TransitionToUbaQ(newUbaQState)
      }
    }
    
    TransitionsTuple(transitions, transitionsToUbaQ)
  }
  
  def copied = new Expander(todo.clone, statesStore, qStatesStore,
      untilsToSatisfy.clone,
      rightsOfUntils.clone,
      literals.clone,
      nexts.clone,
      quantifiers.clone)
  
  def asUbaQuantifierState = {
    new UbaQuantifierState(
        untilsToSatisfy = untilsToSatisfy,
        rightsOfUntils = rightsOfUntils,
        literals = literals,
        nexts = nexts,
        quantifiers = quantifiers,
        statesStore = statesStore,
        qStatesStore = qStatesStore
    )
  }
}

case class TransitionsTuple(transitionsToUba: HashSet[Transition], transitionsToUbaQ: HashSet[TransitionToUbaQ])