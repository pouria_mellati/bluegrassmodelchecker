package bgmc.property

class UnboundVariableEncounteredError(varName: String)
extends RuntimeException(s"Encountered unbound variable: $varName")

class TypingError(msg: String) extends RuntimeException(msg)

class ObjectNotFoundError(objectId: String)
extends RuntimeException(objectId)