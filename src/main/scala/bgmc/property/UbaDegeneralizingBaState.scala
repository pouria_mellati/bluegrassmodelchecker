package bgmc.property

import java.util.{Collection, LinkedList}
import scala.collection.Set
import scala.collection.mutable.{Buffer, ListBuffer, Stack}
import bgmc.{BaState, ModelState}
import bgmc.property.utils.Store
import bgmc.utils.Stats

// Instances of this class are flyweights in the context of a single statesStore.
// Immutable.
class UbaDegeneralizingBaState(val ubaPeer: UbaState, val unacceptingSets: Set[Until], val statesStore: Store[UbaDegeneralizingBaState]) extends BaState {
  case class TransitionToBa(label: Set[BoolExpression], dest: UbaDegeneralizingBaState)
  
  private var isInitialized = false
  private var normalTransitions: Buffer[TransitionToBa] = null 
  private var transitionsToUbaQ: Set[TransitionToUbaQ] = null
  
  override def getSuccessorsFor(modelState: ModelState): Collection[BaState] = {
    if(! isInitialized) {
      val peerTransitions = ubaPeer.transitions
      normalTransitions = degeneralize(peerTransitions.transitionsToUba)        
      transitionsToUbaQ = peerTransitions.transitionsToUbaQ
      isInitialized = true
    }
    
    val degeneralizedQTransitions = degeneralize(quantifierTransitionsToUbaTransitions(transitionsToUbaQ, modelState))
    
    val matchingBaStates = new LinkedList[BaState]()
    for(t <- normalTransitions)
      if(modelStateMatches(modelState, t.label))
        matchingBaStates.add(t.dest)
    for(t <- degeneralizedQTransitions)
      if(modelStateMatches(modelState, t.label))
        matchingBaStates.add(t.dest)
      
//    if(matchingBaStates.isEmpty)
//      throw new RuntimeException("No property successors match a model state.")
    matchingBaStates
  }
  
  private def degeneralize(transitions: Iterable[Transition]): Buffer[TransitionToBa] = {
    val degenedTransitions = ListBuffer.empty[TransitionToBa]
    for(t <- transitions) {
      val succUnacceptingSets = if(unacceptingSets.isEmpty) t.unacceptingSets else (unacceptingSets intersect t.unacceptingSets)
      val createdSucc = new UbaDegeneralizingBaState(t.dest, succUnacceptingSets, statesStore)
      statesStore.getExistingOrNone(createdSucc) match {
        case Some(existingSucc) =>
          degenedTransitions += TransitionToBa(t.label, existingSucc)
        case None =>
          statesStore.add(createdSucc)
          degenedTransitions += TransitionToBa(t.label, createdSucc)
      }
    }
    degenedTransitions
  }
  
  private def quantifierTransitionsToUbaTransitions(qTransitions: Iterable[TransitionToUbaQ], modelState: ModelState): Buffer[Transition] = {
    val transitionsToUba = ListBuffer.empty[Transition]
    val qTransitionsStack = Stack() ++ qTransitions		// Any linked-list would work here.
    while(! qTransitionsStack.isEmpty) {
      val transTuple = qTransitionsStack.pop.dest.transitionsFor(modelState)
      transitionsToUba ++= transTuple.transitionsToUba
      qTransitionsStack pushAll transTuple.transitionsToUbaQ
    }
    transitionsToUba
  }
  
  private def modelStateMatches(modelState: ModelState, labels: Set[BoolExpression]): Boolean =
    labels forall {
      case True => true
      case False => false
      case Neg(bExpr) => ! modelStateMatches(modelState, Set(bExpr))
      case AsBoolExpr(dottedExpr) => dottedExpr.evaluate(modelState).toSingle[Boolean]
      case Comparison(Equality, l, r) => l.evaluate(modelState).toSingle[Any] == r.evaluate(modelState).toSingle[Any]
      case Comparison(Unequality, l, r) => l.evaluate(modelState).toSingle[Any] != r.evaluate(modelState).toSingle[Any]
      
      // TODO: Investigate the correctness of the following two.
      case Comparison(GreaterThan, l, r) => l.evaluate(modelState).toSingle[Comparable[Any]].compareTo(r.evaluate(modelState).toSingle[Comparable[Any]]) > 0
      case Comparison(LessThan, l, r) => l.evaluate(modelState).toSingle[Comparable[Any]].compareTo(r.evaluate(modelState).toSingle[Comparable[Any]]) < 0
    }
  
  override def isFinal(): Boolean =
    unacceptingSets.isEmpty

  override def equals(other: Any) = {
    val that = other.asInstanceOf[UbaDegeneralizingBaState]
    (ubaPeer eq that.ubaPeer) && (unacceptingSets == that.unacceptingSets)
  }
  
  override lazy val hashCode = {
    val prime = 41
    prime * (prime + System.identityHashCode(ubaPeer)) + unacceptingSets.hashCode
  }
  
  def getStats = new Stats().put("numBaStates", statesStore.size).combineWith(ubaPeer.stats)
}