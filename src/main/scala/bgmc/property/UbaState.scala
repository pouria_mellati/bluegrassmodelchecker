package bgmc.property

import collection.mutable.Set
import bgmc.property.utils.Store
import bgmc.utils.Stats

// Instances of this class are flyweights in the context of a single statesStore.
// Immutable => The nexts param should not be mutated after being given to this.
class UbaState(val nexts: Set[Formula], val statesStore: Store[UbaState], val qStatesStore: Store[UbaQuantifierState]) {
  private var _transitions: TransitionsTuple = null
  def transitions = {
    if(_transitions == null)
      _transitions = new Expander(nexts.clone, statesStore, qStatesStore).expand()
    
    _transitions
  }
  
  override def equals(other: Any) = this.nexts == other.asInstanceOf[UbaState].nexts
  override lazy val hashCode = nexts.hashCode
  
  def stats = new Stats().
  	put("numUbaStates", statesStore.size).
  	put("numUbaQuantifierStates", qStatesStore.size)
}