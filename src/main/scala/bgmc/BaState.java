package bgmc;

import bgmc.utils.Stats;
import java.util.Collection;

/*
 * Being a flyweight is a part of the contract of this interface: implementations must 
 * guarantee that for any two BaStates b1 & b1 ever accessible from any BaState (e.g.
 * through invocations of getSuccessorsFor), b1.equals(b2), then b1 and b2 are
 * referentially equal. As a side-effect, BaState objects should be immutable with
 * respect to those of their fields that affect equality.
 * 
 * As such, clients of BaState can base equality on referential equality. 
 */
public interface BaState {
	public Collection<BaState> getSuccessorsFor(ModelState modelState);
	public boolean isFinal();
	public Stats getStats();	// Null is OK to return.
}
