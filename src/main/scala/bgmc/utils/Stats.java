package bgmc.utils;

import java.util.Map;
import java.util.TreeMap;

/* Allows to dynamically gather and combine stats from various places.
 */
public class Stats {
	private Map<String, Object> map = new TreeMap<>();
	
	public Object get(String statName) {
		return map.get(statName);
	}
	
	
	// Mutates and returns this.
	public Stats put(String statName, Object value) {
		map.put(statName, value);
		return this;
	}
	
	public void remove(String statName) {
		map.remove(statName);
	}
	
	// Mutates and returns this.
	public Stats combineWith(Stats statsToInclude) {
		if(statsToInclude != null)
			map.putAll(statsToInclude.map);
		return this;
	}
	
	@Override
	public String toString() {
		return map.toString();
	}
}