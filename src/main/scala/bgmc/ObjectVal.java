package bgmc;

/* Instances will be stored in sets, so implementations must override equals and hashCode if necessary.
 */
public interface ObjectVal {
	// Should return null the field is not found.
	ObjectVal getField(String fieldName);
	
	// TODO: Remove if not needed.
	// TODO: Use an enum.
//	String getType();
	
	Object getValue();
	String getClassId();
	String getId();
}