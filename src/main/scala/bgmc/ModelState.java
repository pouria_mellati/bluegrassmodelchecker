package bgmc;

import java.util.Collection;

/*
 * This is a flyweight interface: duplicate ModelState objects should almost never be returned. Also,
 * ModelState objects should be mostly immutable. See the documentation of the Model interface for more
 * info on how to properly implement this interface.
 */
public interface ModelState {
	Collection<ModelState> getSuccessors();
	Collection<String> getObjectIdsByClassId(String classId);	// TODO: Remove if not needed.
	Collection<ObjectVal> getObjectsByClassId(String classId);
	
	// Should return null if no object with this id is found.
	ObjectVal getObjectById(String objectId);
	
	Collection<String> getEnabledObjectsIds();
	String getLastExecutedObjId();
}
