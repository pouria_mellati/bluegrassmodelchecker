package bgmc;

import bgmc.utils.Stats;
import java.util.Collection;

/*
 * An implementation must guarantee that all ModelState objects returned by calling
 * getInitialStates, and well as those accessible by getting the successors of the
 * returned ModelStates should be 'flyweight's. I.e. for any two ModelState objects
 * m1 & m2 if m1.equals(m2), then m1 and m2 are referentially equal. As a side-effect,
 * once a ModelState is returned, to the outside world, those of its fields that affect
 * equality should remain unchanged.
 * 
 * As such, a client of the Model interface can base equality testing of ModelStates,
 * solely, on referential equality.
 */
public interface Model {
	Collection<ModelState> getInitialStates();
	Stats getStats();	// Null is OK to return.
}
