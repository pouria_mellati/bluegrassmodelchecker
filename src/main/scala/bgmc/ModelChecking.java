package bgmc;

import bgmc.utils.Stats;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ModelChecking {
	private Model model;
	private BaState initPropState;
	
	public ModelChecking(Model model, BaState initPropertyState) {
		this.model = model;
		this.initPropState = initPropertyState;
	}
	
	public ModelCheckingResult run() {
		LinkedList<AlgState> dfsStack = new LinkedList<>();
		AlgStatesStore createdStates = new AlgStatesStore();
		for(ModelState initModelState: model.getInitialStates())
			for(BaState propState: initPropState.getSuccessorsFor(initModelState))
				dfsStack.push(createdStates.addNewOrReturnExisting(new AlgState(initModelState, propState)));
		
		while(!dfsStack.isEmpty()) {
			AlgState dfsState = dfsStack.peek();
			dfsState.wasVisitedByDfs = true;
			
			boolean hasUnexploredSuccessors = false;
			for(AlgState succ: dfsState.getSuccessors(createdStates)) {
				if(!succ.wasVisitedByDfs) {
					dfsStack.push(succ);
					hasUnexploredSuccessors = true;
				}
			}
			
			if(hasUnexploredSuccessors)
				continue;
			
			if(dfsState.isFinal()) {	// Start of cycle check.
				/* Note: This cycle check only accepts fair cycles. I.e. fairness is enforced
				 * algorithmically here.
				 */
				
				// TODO: For performance reasons, the fairness determination process in this cycle check
				// block assumes that once an object becomes active, it won't become inactive, unless it 
				// has executed. This assumption is only true for (most) actor based models.
				
				Collection<String> initiallyEnabledObjs = dfsState.modelState.getEnabledObjectsIds();
				
				LinkedList<AlgState> cycleCheckStack = new LinkedList<>();
				LinkedList<String> executedObjsStack = new LinkedList<>();
				
				for(AlgState initialSucc: dfsState.getSuccessors(createdStates))
					cycleCheckStack.push(initialSucc);
				
				while(! cycleCheckStack.isEmpty()) {
					AlgState cycleCheckState = cycleCheckStack.peek();
					executedObjsStack.push(cycleCheckState.modelState.getLastExecutedObjId());
					cycleCheckState.wasVisitedByCycleCheck = true;
					
					if(cycleCheckState == dfsState) {
						boolean cycleIsFair = true;
						for(String obj: initiallyEnabledObjs)
							if(! executedObjsStack.contains(obj)) {
								cycleIsFair = false;
								break;
							}
						
						if(cycleIsFair) {
							// TODO: Incorporate counter-example into result.
							return createModelCheckingResult(false, createdStates);
						} else {
							cycleCheckStack.pop();
							executedObjsStack.pop();
							continue;
						}
					}
					
					boolean hasUnCycleCheckedSuccessors = false;
					for(AlgState succ: cycleCheckState.getSuccessors(createdStates)) {
						if(! succ.wasVisitedByCycleCheck) {
							cycleCheckStack.push(succ);
							hasUnCycleCheckedSuccessors = true;
						}
					}
					
					if(hasUnCycleCheckedSuccessors)
						continue;
					else {
						cycleCheckStack.pop();
						executedObjsStack.pop();
						continue;
					}
				}
				
				dfsState.wasVisitedByCycleCheck = true;
			}	// End of cycle check.
			dfsStack.pop();
		}
		
		return createModelCheckingResult(true, createdStates);
	}
	
	private ModelCheckingResult createModelCheckingResult(boolean wasModelValid, AlgStatesStore algStatesStore) {
		return new ModelCheckingResult(wasModelValid, new Stats()
			.put("numAlgStates", algStatesStore.getNumStates())
			.combineWith(initPropState.getStats())
			.combineWith(model.getStats()));
	}
}

// TODO: Maybe use a single implementation of XStore throughout the code.
class AlgStatesStore {
	private HashMap<AlgState, AlgState> algStates = new HashMap<>(100000);
	
	AlgState addNewOrReturnExisting(AlgState algState) {
		AlgState stored = algStates.get(algState);
		if(stored == null) {
			algStates.put(algState, algState);
			stored = algState;
		}
		return stored;
	}
	
	int getNumStates() {
		return algStates.size();
	}
}

// TODO: Cache successors.
class AlgState {
	public boolean wasVisitedByDfs = false;
	public boolean wasVisitedByCycleCheck = false;
	
	public ModelState modelState;
	public BaState propState;

	AlgState(ModelState modelState, BaState propState) {
		this.modelState = modelState;
		this.propState = propState;
	}

	boolean isFinal() {
		return propState.isFinal();
	}

	Collection<AlgState> getSuccessors(AlgStatesStore statesStore) {
		Collection<ModelState> modelSuccessors = modelState.getSuccessors();
		
		if(modelSuccessors.isEmpty())
			throw new RuntimeException("Deadlock");		// TODO: Throw something better.
		
		List<AlgState> successors = new LinkedList<>();
		for(ModelState modelSucc: modelSuccessors)
			for(BaState propSucc: propState.getSuccessorsFor(modelSucc))
				successors.add(
					statesStore.addNewOrReturnExisting(new AlgState(modelSucc, propSucc))
				);
		return successors;
	}
	
	@Override
	public boolean equals(Object obj) {
		AlgState that = (AlgState)obj;
		return modelState == that.modelState && propState == that.propState;
	}
	
	@Override
	public int hashCode() {
		return (System.identityHashCode(modelState) * 31) ^ System.identityHashCode(propState);
	}
}