organization := "BlueGrassMC"

name := "BGMC-Core"

moduleName := "BGMC-Core"

version := "0.1.5-SNAPSHOT"

scalaVersion := "2.10.2"

// Prevent the eclipse plugin from creating src/main/java and src/main/scala.

unmanagedSourceDirectories in Compile <<= (scalaSource in Compile)(Seq(_))

unmanagedSourceDirectories in Test <<= (scalaSource in Test)(Seq(_))