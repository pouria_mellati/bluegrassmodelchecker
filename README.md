#### About
This is **BGMC core**, and provides:

* An automata-theoretic model checking algorithm that is compatible with *LeeTL* (an extension of [LTL](http://en.wikipedia.org/wiki/Linear_temporal_logic) with quantification over model objects) formulas.	The algorithm is abstract with respect to the type of model and property it uses: A model must implement a *Model* interface and the property has to implement a *Buechi Automata* interface (actual interface names might vary).
* A mechanism for handling LeeTL formulas in model checking: given the AST of a LeeTL formula in negative normal form (an instance of `Formula`), masquerades it as a Buechi Automata. In other words, an implementation of a BA property is provided for LeeTL formulas.

#### Related Projects
* [**BGMC Model Helpers**](https://bitbucket.org/pouria_mellati/bgmc-model-helpers): An easier way to create actor-based models for use with BGMC core.
* [**BGMC Property Parser**](https://bitbucket.org/pouria_mellati/bgmc-property-parser): Parses a LeeTL property file to return a `Formula` compatible with BGMC core.
* [**DynamicRebecaToBgmc**](https://bitbucket.org/pouria_mellati/dynamicrebecatobgmc): Compiles a [rebeca](http://www.rebeca-lang.org/) source file (augmented with support for dynamicism) into a set of Java files, making up a complete BGMC model.